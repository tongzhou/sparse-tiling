#ifndef MATRICES_H
#define MATRICES_H

#include <cstdlib>
#include <cstdio>
#include <cstring>

#define TRUE 1
#define FALSE 0

#define perror(msg, ...) fprintf(stderr, "ERROR: %s:%d - " msg "\n", __func__, __LINE__, ##__VA_ARGS__); \
                    exit(EXIT_FAILURE);


typedef struct csr_matrix {
    int64_t nRow, nCol, nnz;

    int64_t *rowptr;
    int64_t *cols;
    int64_t *vals;
} csr_t;

typedef struct dense_matrix {
    int64_t nRow, nCol;

    int64_t *vals;
} dense_matrix;

csr_t *read_mm_matrix(const char *filename);

dense_matrix *create_dense_array(int64_t nRows, int64_t nCols);

#endif // MATRICES_H
