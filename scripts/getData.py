
import urllib.request
import os
import tarfile
import shutil
# import wget

# Matrices that will be downloaded
Matrices = [

    'https://suitesparse-collection-website.herokuapp.com/MM/Um/2cubes_sphere.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/vanHeukelum/cage12.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Williams/cant.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Williams/consph.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Williams/cop20k_A.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Oberwolfach/filter3D.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/GHS_psdef/hood.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/JGD_Homology/m133-b3.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Williams/mac_econ_fwd500.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/QLi/majorbasis.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Williams/mc2depi.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Um/offshore.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Pajek/patents_main.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Williams/pdb1HYS.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/FEMLAB/poisson3Da.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Boeing/pwtk.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Bova/rma10.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Hamm/scircuit.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/DNVS/shipsec1.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Williams/webbase-1M.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/SNAP/web-BerkStan.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/Simon/appu.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/FIDAP/ex11.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/FEMLAB/sme3Dc.tar.gz',
    'https://suitesparse-collection-website.herokuapp.com/MM/TSOPF/TSOPF_RS_b300_c1.tar.gz',
]

# Make the data directory
if not os.path.exists('data'):
    os.makedirs('data')

if not os.path.exists('matrices'):
    os.makedirs('matrices')

if not os.path.exists('temp'):
    os.makedirs('temp')

# Download files
for m in Matrices:

    firstPos = m.rfind('/')
    name = m[firstPos + 1:]

    print("Downloading: ", 'data/' + name)
    urllib.request.urlretrieve(m, 'data/' + name)



# untar all files -- move to the Matrices directory
for path, directories, files in os.walk('./data'):
    for f in files:
        if f.endswith('.tar.gz'):
            print(f)
            tar = tarfile.open(os.path.join(path, f), 'r:gz')
            tar.extractall(path='./temp')
            tar.close()


# move matrices to a directory
for path, directories, _ in os.walk('./temp'):
    for d in directories:
        # print(d)
        for _, _, files in os.walk(os.path.join(path, d)):
            # print(files)
            for f in files:
                if (f == d + ".mtx"):
                # if f.endswith('.mtx'):
                    # print(os.path.join(path, d, f))
                    # print('./matrices/' + f)
                    shutil.copy(os.path.join(path, d, f), './matrices/' + f)

# cleanup
try:
    shutil.rmtree('./temp')
    shutil.rmtree('./data')
    shutil.move('./matrices', '../matrices')
except OSError as e:
    print("Error: %s" % e.strerror)

