
#include "matrices.h"
#include "mmio.h"

#include <cstdlib>
#include <cstdio>
#include <cstring>

// Read a symmetric or unsymmetric square matrix in the matrix market format
// and return a CSR matrix of that
csr_t *read_mm_matrix(const char *filename)
{
    if (!filename || strlen(filename) < 1) { perror("Filename Empty"); }

    FILE *f;
    if ((f = fopen(filename, "r")) == NULL) { perror("Could not open file"); }
    MM_typecode matcode;

    if (mm_read_banner(f, &matcode)) { perror("File is not in the matrix market format"); }

    if (!(mm_is_matrix(matcode) && mm_is_sparse(matcode))) { perror("Matrix Type not supported: %s", matcode); }

    // csr_t *csr = malloc(sizeof(csr_t));
    csr_t *csr = new csr_t;
    if (!csr) { perror("Could not allocated csr matrix"); }

    if (mm_read_mtx_crd_size(f, (int *) &csr->nRow, (int *) &csr->nCol, (int *) &csr->nnz)) { perror("Could not parse matrix dimensions"); }

    if (csr->nRow != csr->nCol) { perror("Don't support non-square matrices"); }

    // csr->rowptr = (int64_t *) allocate_clean(sizeof(int64_t), (csr->m + 1) , HIGH);
    csr->rowptr = new int64_t[csr->nRow + 1];
    if (!csr->rowptr) { perror("Could not allocate rowptr memory"); }

    int is_pattern = FALSE;
    int is_symmetric = FALSE;

    // allocate space for colss and vals
    if (mm_is_symmetric(matcode)) {
        // allocate twice the vals

        // csr->cols = allocate(sizeof(int64_t) * csr->nnz * 2, HIGH);
        csr->cols = new int64_t[csr->nnz * 2];
        if (!mm_is_pattern(matcode)) {
            // csr->vals = allocate(sizeof(double) * csr->nnz * 2, HIGH);
            csr->vals = new int64_t[csr->nnz * 2];
        } else {
            is_pattern = TRUE;
        }
        is_symmetric = TRUE;
    } else {
        csr->cols = new int64_t[csr->nnz];
        // csr->cols = allocate(sizeof(int64_t) * csr->nnz, HIGH);
        if (!mm_is_pattern(matcode)) {
            csr->vals = new int64_t[csr->nnz];
            // csr->vals = allocate(sizeof(double) * csr->nnz, HIGH);
        } else {
            is_pattern = TRUE;
        }
    }


    long data_location = ftell(f);

    // Finding row lengths for each row
    for (int64_t i = 0; i < csr->nnz; i++) {

        int I, J;
        double vals;
        mm_read_mtx_crd_entry(f, &I, &J, &vals, NULL, matcode);
        I--; J--; // Fix to zero base indexing

        csr->rowptr[I + 1]++;
        if (is_symmetric) {
            csr->rowptr[J + 1]++; // for the symmetric portion
        }
    }

    // Set offsets for each row
    for (int64_t i = 0; i < csr->nRow; i++) {
        csr->rowptr[i + 1] += csr->rowptr[i];
    }


    fseek(f, data_location, SEEK_SET);
    for (int64_t i = 0; i < csr->nnz; i++) {
        int I, J;
        double vals;
        mm_read_mtx_crd_entry(f, &I, &J, &vals, NULL, matcode);
        I--; J--; // Fix to zero base indexing

        int64_t t = csr->rowptr[I]; // pointer to colssumn in row
        if (!is_pattern) {
            csr->vals[t] = vals;
        }
        csr->cols[t] = J;
        csr->rowptr[I]++; // update row pointer to next cell

        if (is_symmetric) {
            // row and cols are swapped
            t = csr->rowptr[J]; // pointer to colsumn in row for symmetry
            if (!is_pattern) {
                csr->vals[t] = vals;
            }
            csr->cols[t] = I;
            csr->rowptr[J]++; // update row pointer to next cell
        }
    }

    for (int64_t i = csr->nRow; i > 0; i--) { // readjust row pointers
        csr->rowptr[i] = csr->rowptr[i - 1];
    }
    csr->rowptr[0] = 0;

    if (is_symmetric) {
        csr->nnz *= 2; // Symmetry means double the non zeros
    }

    fclose(f);
    return csr;
}

dense_matrix* create_dense_array(int64_t nRows, int64_t nCols)
{
    dense_matrix *d = new dense_matrix;
    if (!d) { perror ("Allocation Error: Could not allocate dense matrix"); }
    d->nRow = nRows; d->nCol = nCols;

    d->vals = new int64_t[nRows * nCols];
    if (!d->vals) { perror("Allocation Error: Could not allocate dense matrix data store"); }

    // Populate array
    for (int64_t i = 0; i < d->nRow * d->nCol; i++) {
        d->vals[i] = std::rand();
    }

    return d;
}

// Generate and return a random CSR matrix with the specified density of non-zeros
csr_t* get_csr_matrix(int64_t nRows, int64_t nCols, double density = 0.10)
{

}