
#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>

#include "papi.h"
#include "matrices.h"

// #define PROFILE_L1 1

// PAPI Constants

int EventSet;
const int NUM_EVENTS = 3;

// Event list in PAPI format
                                    // L1 misses, Total LD/ST Insts, Cycle count, L3 accesses, L3 misses

#ifdef PROFILE_L1
int PAPI_EVENT_CODES[NUM_EVENTS] = {PAPI_L1_DCM, PAPI_LST_INS, PAPI_TOT_CYC}; // PAPI_L3_TCA, PAPI_L3_TCM};
#else
int PAPI_EVENT_CODES[NUM_EVENTS] = {PAPI_L3_TCA, PAPI_L3_TCM, PAPI_TOT_CYC}; // Alternate events sequence
#endif

// int PAPI_EVENT_CODES[NUM_EVENTS] = {PAPI_L3_TCM, PAPI_L3_TCA, PAPI_TOT_CYC}; // PAPI_L3_DCR};

// Use this for reading the counter values
long long VALUES[NUM_EVENTS];

// Initialize PAPI and return the eventset which can be invoked as required
void init_papi_and_setup_events()
{
    EventSet = PAPI_NULL;
    int retval;

    if ((retval = PAPI_library_init(PAPI_VER_CURRENT)) != PAPI_VER_CURRENT) {
        perror("Could not init PAPI: %d\n", retval);
    }

    // Creating event set
    if ((retval = PAPI_create_eventset(&EventSet)) != PAPI_OK) {
        perror("Could not create PAPI event set: %d\n", retval);
    }

    if ((retval = PAPI_add_events(EventSet, PAPI_EVENT_CODES, NUM_EVENTS)) != PAPI_OK) {
        perror("Could not add events to the eventset: %d\n", retval);
    }

}

// Start the event counters
inline void start_papi_events(int EventSet)
{
    int retval;
    if ( (retval = PAPI_start(EventSet)) != PAPI_OK ) {
        perror("Could not start counter collection\n");
    }
}

// Stop and read the event counters
inline void stop_papi_events(int EventSet, long long *values)
{
    int retval;
    if ((retval = PAPI_stop(EventSet, values)) != PAPI_OK) {
        perror("Could not read the counter values");
    }
}

void clear_papi_events()
{
    int retval;
    if ((retval = PAPI_remove_events(EventSet, PAPI_EVENT_CODES, NUM_EVENTS)) != PAPI_OK) {
        perror("Could not remove events from eventset");
    }

    if ((retval = PAPI_destroy_eventset(&EventSet)) != PAPI_OK) {
        perror("Could not free PAPI EventSet data structures");
    }

    // Free PAPI resources
    PAPI_shutdown();
}

void spmm_sparse_dense(struct csr_matrix *s, struct dense_matrix *d, struct dense_matrix *C)
{

    C->vals = new int64_t[s->nRow * d->nCol];
    C->nCol = d->nCol;
    C->nRow = s->nRow;

    start_papi_events(EventSet);

    for (int64_t i = 0; i < s->nRow; i++) {
        int64_t col_start = s->rowptr[i];
        int64_t col_end = s->rowptr[i + 1];
        for (int64_t ki = col_start; ki < col_end; ki++) {
            int64_t k = s->cols[ki];
            for (int64_t j = 0; j < d->nCol; j++) {
                C->vals[i * C->nCol + j] += s->vals[ki] * d->vals[k * d->nCol + j];
            }
        }
    }

    stop_papi_events(EventSet, VALUES);

}

void spmm_sparse_dense_dense_tiled(struct csr_matrix *s, struct dense_matrix *d, struct dense_matrix *C, int64_t TJ)
{
    C->vals = new int64_t[s->nRow * d->nCol];
    C->nCol = d->nCol;
    C->nRow = s->nRow;

    start_papi_events(EventSet);

    for (int64_t jj = 0; jj < d->nCol; jj += TJ) {
        for (int64_t i = 0; i < s->nRow; i++) {
            int64_t col_start = s->rowptr[i];
            int64_t col_end = s->rowptr[i + 1];

            for (int64_t ki = col_start; ki < col_end; ki++) {
                int k = s->cols[ki];

                for (int64_t j = jj; j < std::min(jj + TJ, d->nCol); j++) {
                    C->vals[i * C->nCol + j] += s->vals[ki] * d->vals[k * d->nCol + j];
                }
            }
        }
    }

    stop_papi_events(EventSet, VALUES);

}


void spmm_sparse_dense_sparse_tiled1(struct csr_matrix *s, struct dense_matrix *d, struct dense_matrix *C, int64_t TJ, int64_t TK)
{
    C->vals = new int64_t[s->nRow * d->nCol];
    C->nCol = d->nCol;
    C->nRow = s->nRow;

    // nextKi -- initialized as the first column of every row in the sparse matrix
    int64_t *nextKi = new int64_t[s->nRow+1];
    for (int64_t i = 0; i < s->nRow+1; i++) {
        nextKi[i] = s->rowptr[i];
    }

    start_papi_events(EventSet);

    for (int64_t kk = 0; kk < d->nCol; kk += TK) {
        for (int64_t jj = 0; jj < d->nCol; jj += TJ) {

            for (int64_t i = 0; i < s->nRow; i++) {
                int64_t col_start = s->rowptr[i];
                int64_t col_end = s->rowptr[i + 1];
                
                int64_t ki = nextKi[i];
                for (; (s->cols[ki] < kk + TK) && (ki < nextKi[i+1]); ki++) {
                    int64_t k = s->cols[ki];
                    
                    for (int64_t j = jj; j < std::min(d->nCol, jj + TJ); j++) {
                        C->vals[i * C->nCol + j] += s->vals[ki] * d->vals[k * d->nCol + j];
                    }
                }
		nextKi[i] = ki;
            }
        }
    }

    stop_papi_events(EventSet, VALUES);

}

static void clear_sparse_matrix(struct csr_matrix *s)
{
    if (s) {
        if (s->cols) { delete[] s->cols; }
        if (s->rowptr) { delete[] s->rowptr; }
        if (s->vals) { delete[] s->vals; }

        delete s;
    }
}

int main(int argc, char *argv[])
{
    if (argc < 3) {

        std::cerr << "Usage: ./spmm <matrix name> <1-4: algorithm> <TJ> <TK> <repeat count>" << std::endl;
        std::cerr << "1 - baseline" << std::endl;
        std::cerr << "2 - i loop tiled" << std::endl;
        std::cerr << "1 - i,j loop tiled" << std::endl;
        std::cerr << "1 - i,j,k loop tiled" << std::endl; 

        std::cerr << "Need a matrix name to run experiments" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    int64_t TJ = 1, TK = 1, TI = 1, REPEAT_COUNT = 11; 
    if (argc >= 4) { TJ = std::atoi(argv[3]); }
    if (argc >= 5) { TK = std::atoi(argv[4]); }
    if (argc >= 6) { TI = std::atoi(argv[5]); }
    if (argc >= 7) { REPEAT_COUNT = std::atoi(argv[6]); }

    csr_t *csr_mat = read_mm_matrix(argv[1]);
    dense_matrix *dense_mat = create_dense_array(csr_mat->nRow, csr_mat->nCol);
    dense_matrix res_mat;

    // SETUP PAPI
    init_papi_and_setup_events();

    std::vector<long long> cache_accesses;
    std::vector<long long> cache_misses;
    std::vector<long long> cycle_counts;
    std::vector<long long> L3_cache_accesses;
    std::vector<long long> L3_cache_misses;

    for (int64_t i = 0; i < REPEAT_COUNT; i++) {
    // Call the matrix mutliplication function
        switch (std::atoi(argv[2])) {
        case 1:
            spmm_sparse_dense(csr_mat, dense_mat, &res_mat);
            break;
        case 2:
            
            spmm_sparse_dense_dense_tiled(csr_mat, dense_mat, &res_mat, TJ);
            break;
        case 3:
            spmm_sparse_dense_sparse_tiled1(csr_mat, dense_mat, &res_mat, TJ, TK);

        case 4:
            // call the new function which should take TI along with the other 
            break;
        
        default:
            break;
        }

        // Collect runtime counter data
        #ifdef PROFILE_L1
        cache_accesses.push_back(VALUES[1]);
        cache_misses.push_back(VALUES[0]);
        #else
        L3_cache_accesses.push_back(VALUES[0]);
        L3_cache_misses.push_back(VALUES[1]);
        #endif

        cycle_counts.push_back(VALUES[2]);

        // L3_cache_accesses.push_back(VALUES[3]);
        // L3_cache_misses.push_back(VALUES[4]);

    }

    // Print the experiment outcome in terms of cache accesses and misses
    // long long avg_cache_accesses = std::accumulate(cache_accesses.begin(), cache_accesses.end(), 0) / REPEAT_COUNT;
    // long long avg_cache_misses = std::accumulate(cache_misses.begin(), cache_accesses.end(), 0) / REPEAT_COUNT;
    // long long avg_cycle_count = std::accumulate(cycle_counts.begin(), cache_accesses.end(), 0) / REPEAT_COUNT;
    // long long avg_l3_cache_accesses = std::accumulate(L3_cache_accesses.begin(), cache_accesses.end(), 0) / REPEAT_COUNT;
    // long long avg_l3_cache_misses = std::accumulate(L3_cache_misses.begin(), cache_accesses.end(), 0) / REPEAT_COUNT;

    #ifdef PROFILE_L1
    std::cout << "L1 Cache Accesses,L1 Cache Misses,Cycle Count" << std::endl;
    #else
    std::cout << "L3 Cache accesses, L3 Cache Misses,Cycle Count" << std::endl;
    #endif

    for (int64_t i = 0; i < REPEAT_COUNT; i++) {
        
        #ifdef PROFILE_L1
        std::cout << cache_accesses[i] << "," << cache_misses[i] << "," << cycle_counts[i] << std::endl;
        #else
        std::cout << L3_cache_accesses[i] << "," << L3_cache_misses[i] << "," << cycle_counts[i] << std::endl;
        #endif
    }

    clear_papi_events();

    return 0;
}
